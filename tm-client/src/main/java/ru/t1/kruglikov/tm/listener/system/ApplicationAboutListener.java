package ru.t1.kruglikov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.kruglikov.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.kruglikov.tm.event.ConsoleEvent;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Show about developer.";

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");

        @NotNull final ApplicationAboutResponse response = systemEndpoint.getAbout(new ApplicationAboutRequest());
        @NotNull final String name = response.getName();
        @NotNull final String email = response.getEmail();

        System.out.println("Name: " + name);
        System.out.println("Email: " + email);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
