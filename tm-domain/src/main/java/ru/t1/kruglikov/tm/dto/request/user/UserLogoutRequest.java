package ru.t1.kruglikov.tm.dto.request.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(@Nullable String token) {
        super(token);
    }

}
