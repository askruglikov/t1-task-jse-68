package ru.t1.kruglikov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.enumerated.UserSort;

import java.util.List;

@Repository
@Scope("prototype")
public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    @Query("SELECT p FROM UserDTO p ORDER BY :sort")
    List<UserDTO> findAll(@Nullable String sort);

}