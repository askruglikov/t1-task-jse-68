package ru.t1.kruglikov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.kruglikov.tm.model.AbstractModel;

import java.util.List;

@Repository
@Scope("prototype")
public interface IRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}
