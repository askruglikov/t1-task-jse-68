package ru.t1.kruglikov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.enumerated.UserSort;
import ru.t1.kruglikov.tm.model.User;

import java.util.List;

@Repository
@Scope("prototype")
public interface IUserRepository extends IRepository<User> {

    @Nullable
    @Query("SELECT p FROM UserDTO p ORDER BY :sort")
    List<User> findAll(@Nullable String sort);

}
