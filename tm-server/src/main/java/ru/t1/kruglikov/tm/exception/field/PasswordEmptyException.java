package ru.t1.kruglikov.tm.exception.field;

public class PasswordEmptyException extends AbstractFieldExceprion {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
